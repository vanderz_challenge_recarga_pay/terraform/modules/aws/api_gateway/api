resource "aws_api_gateway_rest_api" "main" {
  name        = var.api_name

  dynamic "endpoint_configuration" {
    for_each = var.endpoint_configuration
    content {
      types = endpoint_configuration.value.types
    }
  }

}
